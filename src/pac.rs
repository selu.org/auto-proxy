use cfg_if::cfg_if;
use chrono::prelude::*;
use duktape_ffi_raw::{
    duk_compile_raw, duk_context, duk_create_heap, duk_destroy_heap, duk_dup_top,
    duk_get_prop_string, duk_get_string, duk_get_top, duk_get_type, duk_pcall, duk_pop, duk_pop_n,
    duk_push_boolean, duk_push_c_function, duk_push_int, duk_push_string, duk_put_global_string,
    duk_ret_t, DUK_COMPILE_FUNCTION, DUK_COMPILE_NOFILENAME, DUK_COMPILE_NOSOURCE,
    DUK_COMPILE_SAFE, DUK_COMPILE_STRLEN,
};
use hyper::Uri;
use lazy_static::lazy_static;
use log::debug;
use regex::Regex;
use std::error::Error;
use std::ffi::{CStr, CString};
use std::fmt;
use std::net::Ipv4Addr;
use std::ptr::null_mut;
use std::str::FromStr;

pub mod ch {
    use chrono::prelude::*;
    pub struct Chrono {}
    #[cfg_attr(test, mockall::automock)]
    impl Chrono {
        pub fn utc() -> DateTime<Utc> {
            Utc::now()
        }

        pub fn local() -> DateTime<Local> {
            Local::now()
        }
    }
}

cfg_if! {
    if #[cfg(test)] {
        use self::ch::MockChrono as Chrono;
    } else {
        use self::ch::Chrono;
    }
}

pub struct PAC {
    ctx: *mut duk_context,
    fcode: Option<String>,
}

unsafe impl Send for PAC {}

#[derive(Debug)]
pub enum Proxy {
    Direct,
    Proxy(Uri),
    Socks(Uri),
    Http(Uri),
    Https(Uri),
    Socks4(Uri),
    Socks5(Uri),
}

impl FromStr for Proxy {
    type Err = PACError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        lazy_static! {
            static ref RE: Regex = Regex::new(r"(?P<mode>[A-Z]+)(?: +(?P<uri>.*))?").unwrap();
        }
        let parts = RE.captures(s).ok_or(PACError {
            message: "invalid proxy rule".to_string(),
        })?;
        let uri = match parts.name("uri") {
            Some(u) => Some(u.as_str().parse().map_err(|_| PACError {
                message: "invalid proxy uri".to_string(),
            })?),
            None => None,
        };
        match parts
            .name("mode")
            .ok_or(PACError {
                message: "missing proxy mode".to_string(),
            })?
            .as_str()
        {
            "DIRECT" => Ok(Proxy::Direct),
            "PROXY" => Ok(Proxy::Proxy(uri.ok_or(PACError {
                message: "missing proxy uri".to_string(),
            })?)),
            _ => Err(PACError {
                message: "invalid proxy mode".to_string(),
            }),
        }
    }
}

#[derive(Debug)]
pub struct PACError {
    message: String,
}

impl fmt::Display for PACError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "PAC Error: {}", self.message)
    }
}

impl Error for PACError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        None
    }
}

static FUNCTIONS: [(
    unsafe extern "C" fn(*mut duk_context) -> duk_ret_t,
    &str,
    i32,
); 10] = [
    (pac_is_plain_host_name, "isPlainHostName", 1),
    (pac_dns_domain_is, "dnsDomainIs", 2),
    (pac_is_resolvable, "isResolvable", 1),
    (pac_dns_resolve, "dnsResolve", 1),
    (pac_dns_domain_levels, "dnsDomainLevels", 1),
    (pac_my_ip_address, "myIpAddress", 0),
    (pac_is_in_net, "isInNet", 3),
    (pac_sh_exp_match, "shExpMatch", 2),
    (pac_local_host_or_domain_is, "localHostOrDomainIs", 2),
    (pac_weekday_range, "weekdayRange", -1),
    // dateRange
    // timeRange
];

unsafe extern "C" fn pac_is_plain_host_name(ctx: *mut duk_context) -> duk_ret_t {
    let host = CStr::from_ptr(duk_get_string(ctx, 0)).to_string_lossy();
    duk_push_boolean(ctx, if host.contains(".") { 0 } else { 1 });
    1
}

unsafe extern "C" fn pac_dns_domain_is(ctx: *mut duk_context) -> duk_ret_t {
    let host = CStr::from_ptr(duk_get_string(ctx, 0)).to_string_lossy();
    let domain = CStr::from_ptr(duk_get_string(ctx, 1)).to_string_lossy();
    duk_push_boolean(
        ctx,
        if host.ends_with(&domain.into_owned()) {
            1
        } else {
            0
        },
    );
    1
}

unsafe extern "C" fn pac_is_resolvable(ctx: *mut duk_context) -> duk_ret_t {
    let host = CStr::from_ptr(duk_get_string(ctx, 0)).to_string_lossy();
    if let Ok(addrs) = dns_lookup::lookup_host(&host) {
        duk_push_boolean(ctx, if addrs.len() == 0 { 0 } else { 1 });
    } else {
        duk_push_boolean(ctx, 0);
    }
    1
}

unsafe extern "C" fn pac_dns_resolve(ctx: *mut duk_context) -> duk_ret_t {
    let host = CStr::from_ptr(duk_get_string(ctx, 0)).to_string_lossy();
    let ip = if let Ok(addrs) = dns_lookup::lookup_host(&host) {
        if let Some(addr) = addrs
            .iter()
            .filter(|a| a.is_ipv4())
            .map(|a| a.to_string())
            .next()
        {
            addr
        } else {
            String::from("")
        }
    } else {
        String::from("")
    };
    duk_push_string(ctx, CString::new(ip).expect("CString").into_raw());
    1
}

unsafe extern "C" fn pac_dns_domain_levels(ctx: *mut duk_context) -> duk_ret_t {
    let host = CStr::from_ptr(duk_get_string(ctx, 0)).to_string_lossy();
    duk_push_int(ctx, host.chars().filter(|c| *c == '.').count() as i32);
    1
}

unsafe extern "C" fn pac_my_ip_address(ctx: *mut duk_context) -> duk_ret_t {
    let ip = if let Ok(ifs) = get_if_addrs::get_if_addrs() {
        if let Some(ip) = ifs
            .into_iter()
            .filter(|i| !i.is_loopback() && i.ip().is_ipv4())
            .map(|i| i.ip().to_string())
            .next()
        {
            ip
        } else {
            String::from("")
        }
    } else {
        String::from("")
    };
    duk_push_string(ctx, CString::new(ip).expect("CString").into_raw());
    1
}

unsafe extern "C" fn pac_is_in_net(ctx: *mut duk_context) -> duk_ret_t {
    let host =
        Ipv4Addr::from_str(&CStr::from_ptr(duk_get_string(ctx, 0)).to_string_lossy()).unwrap();
    let network =
        Ipv4Addr::from_str(&CStr::from_ptr(duk_get_string(ctx, 1)).to_string_lossy()).unwrap();
    let netmask = (!u32::from_be_bytes(
        Ipv4Addr::from_str(&CStr::from_ptr(duk_get_string(ctx, 2)).to_string_lossy())
            .unwrap()
            .octets(),
    ))
    .leading_zeros() as u8;
    let net = ipnet::Ipv4Net::new(network, netmask).unwrap();
    duk_push_boolean(ctx, if net.contains(&host) { 1 } else { 0 });
    1
}

unsafe extern "C" fn pac_sh_exp_match(ctx: *mut duk_context) -> duk_ret_t {
    let host = CStr::from_ptr(duk_get_string(ctx, 0)).to_string_lossy();
    let pattern =
        glob::Pattern::new(&CStr::from_ptr(duk_get_string(ctx, 1)).to_string_lossy()).unwrap();
    duk_push_boolean(ctx, if pattern.matches(&host) { 1 } else { 0 });
    1
}

unsafe extern "C" fn pac_local_host_or_domain_is(ctx: *mut duk_context) -> duk_ret_t {
    let host = CStr::from_ptr(duk_get_string(ctx, 0)).to_string_lossy();
    let domain = CStr::from_ptr(duk_get_string(ctx, 1)).to_string_lossy();
    let result = if !host.contains(".") {
        host == domain.split(".").next().unwrap()
    } else {
        host == domain
    };
    duk_push_boolean(ctx, if result { 1 } else { 0 });
    1
}

unsafe extern "C" fn pac_weekday_range(ctx: *mut duk_context) -> duk_ret_t {
    let mut nargs = duk_get_top(ctx);
    let gmt = duk_get_type(ctx, -1) == 5
        && CStr::from_ptr(duk_get_string(ctx, -1))
            .to_string_lossy()
            .to_uppercase()
            == "GMT";
    if gmt {
        nargs -= 1;
    }
    let weekday = if gmt {
        Chrono::utc().weekday()
    } else {
        Chrono::local().weekday()
    };
    let result = match nargs {
        1 => {
            let wd = Weekday::from_str(&CStr::from_ptr(duk_get_string(ctx, 0)).to_string_lossy())
                .unwrap();
            weekday == wd
        }
        2 => {
            let wd1 = Weekday::from_str(&CStr::from_ptr(duk_get_string(ctx, 0)).to_string_lossy())
                .unwrap()
                .number_from_monday();
            let wd2 = Weekday::from_str(&CStr::from_ptr(duk_get_string(ctx, 1)).to_string_lossy())
                .unwrap()
                .number_from_monday();
            let weekday = weekday.number_from_monday();
            if wd1 <= wd2 {
                wd1 <= weekday && weekday <= wd2
            } else {
                weekday <= wd1 || wd2 <= weekday
            }
        }
        _ => false,
    };
    duk_push_boolean(ctx, if result { 1 } else { 0 });
    1
}

impl PAC {
    pub fn new() -> Self {
        Self {
            ctx: unsafe {
                let ctx = duk_create_heap(
                    Option::None,
                    Option::None,
                    Option::None,
                    null_mut(),
                    Option::None,
                );
                for (func, name, nargs) in FUNCTIONS.iter() {
                    duk_push_c_function(ctx, Some(*func), *nargs);
                    duk_put_global_string(ctx, CString::new(*name).unwrap().into_raw());
                }
                debug!("duktape context is created");
                ctx
            },
            fcode: None,
        }
    }

    pub fn setup(&mut self, function: &str) -> Result<(), Box<dyn Error>> {
        match &self.fcode {
            Some(s) => {
                if s == function {
                    debug!("function is unchanged");
                    return Ok(());
                }
            }
            None => (),
        }
        let code = CString::new(function).map_err(|e| PACError {
            message: format!("function string is wrong: {}", e).to_string(),
        })?;
        unsafe {
            let elems = duk_get_top(self.ctx);
            if elems > 0 {
                duk_pop_n(self.ctx, elems);
            }
            let res = duk_compile_raw(
                self.ctx,
                code.into_raw(),
                0,
                0 | DUK_COMPILE_FUNCTION
                    | DUK_COMPILE_STRLEN
                    | DUK_COMPILE_NOFILENAME
                    | DUK_COMPILE_SAFE
                    | DUK_COMPILE_NOSOURCE,
            );
            if res != 0 {
                duk_pop(self.ctx);
                return Err(Box::new(PACError {
                    message: "function compile error".to_string(),
                }));
            }
        }
        self.fcode = Some(String::from(function));
        debug!("proxy function is compiled");
        Ok(())
    }

    pub fn call<'a>(&mut self, url: &str, host: &str) -> Result<Vec<Proxy>, Box<dyn Error>> {
        lazy_static! {
            static ref SPLITTER: Regex = Regex::new(r"; *").unwrap();
        }

        unsafe {
            if duk_get_top(self.ctx) == 0 {
                return Err(Box::new(PACError {
                    message: "proxy is not set up".to_string(),
                }));
            }
            duk_dup_top(self.ctx);
            duk_push_string(self.ctx, CString::new(url).expect("CString").into_raw());
            duk_push_string(self.ctx, CString::new(host).expect("CString").into_raw());
            if duk_pcall(self.ctx, 2) > 0 {
                duk_get_prop_string(self.ctx, -1, CString::new("stack").unwrap().into_raw());
                Err(Box::new(PACError {
                    message: CStr::from_ptr(duk_get_string(self.ctx, -1))
                        .to_string_lossy()
                        .into_owned(),
                }))
            } else {
                let res = CStr::from_ptr(duk_get_string(self.ctx, -1))
                    .to_string_lossy()
                    .into_owned();
                duk_pop(self.ctx);
                debug!("proxy function ran successfully");
                Ok(SPLITTER.split(&res).map(|s| s.parse().unwrap()).collect())
            }
        }
    }

    pub fn status(&mut self) -> () {
        unsafe {
            debug!("top: {}", duk_get_top(self.ctx));
        }
    }
}

impl Drop for PAC {
    fn drop(&mut self) {
        unsafe {
            duk_destroy_heap(self.ctx);
            debug!("duktape context is dropped");
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use duktape_ffi_raw::{duk_eval_raw, duk_get_boolean, duk_get_int, DUK_COMPILE_EVAL};
    use lazy_static::lazy_static;

    lazy_static! {
        static ref TEST_LOCAL_MUTEX: std::sync::Mutex<()> = std::sync::Mutex::new(());
        static ref TEST_UTC_MUTEX: std::sync::Mutex<()> = std::sync::Mutex::new(());
    }

    #[test]
    fn test_is_plain_host_true() {
        let pac = PAC::new();
        let res = unsafe {
            duk_eval_raw(
                pac.ctx,
                CString::new("isPlainHostName(\"alma\")")
                    .unwrap()
                    .into_raw(),
                0,
                0 | DUK_COMPILE_EVAL
                    | DUK_COMPILE_STRLEN
                    | DUK_COMPILE_NOFILENAME
                    | DUK_COMPILE_SAFE
                    | DUK_COMPILE_NOSOURCE,
            )
        };
        assert_eq!(res, 0); // success
        assert_eq!(unsafe { duk_get_type(pac.ctx, -1) }, 3); // boolean
        assert_eq!(unsafe { duk_get_boolean(pac.ctx, -1) }, 1);
    }

    #[test]
    fn test_is_plain_host_false() {
        let pac = PAC::new();
        let res = unsafe {
            duk_eval_raw(
                pac.ctx,
                CString::new("isPlainHostName(\"alma.hu\")")
                    .unwrap()
                    .into_raw(),
                0,
                0 | DUK_COMPILE_EVAL
                    | DUK_COMPILE_STRLEN
                    | DUK_COMPILE_NOFILENAME
                    | DUK_COMPILE_SAFE
                    | DUK_COMPILE_NOSOURCE,
            )
        };
        assert_eq!(res, 0); // success
        assert_eq!(unsafe { duk_get_type(pac.ctx, -1) }, 3); // boolean
        assert_eq!(unsafe { duk_get_boolean(pac.ctx, -1) }, 0);
    }

    #[test]
    fn test_dns_domain_is_true() {
        let pac = PAC::new();
        let res = unsafe {
            duk_eval_raw(
                pac.ctx,
                CString::new("dnsDomainIs(\"alma.hu\",\".hu\")")
                    .unwrap()
                    .into_raw(),
                0,
                0 | DUK_COMPILE_EVAL
                    | DUK_COMPILE_STRLEN
                    | DUK_COMPILE_NOFILENAME
                    | DUK_COMPILE_SAFE
                    | DUK_COMPILE_NOSOURCE,
            )
        };
        assert_eq!(res, 0); // success
        assert_eq!(unsafe { duk_get_type(pac.ctx, -1) }, 3); // boolean
        assert_eq!(unsafe { duk_get_boolean(pac.ctx, -1) }, 1);
    }

    #[test]
    fn test_dns_domain_is_false() {
        let pac = PAC::new();
        let res = unsafe {
            duk_eval_raw(
                pac.ctx,
                CString::new("dnsDomainIs(\"alma.hu\",\".com\")")
                    .unwrap()
                    .into_raw(),
                0,
                0 | DUK_COMPILE_EVAL
                    | DUK_COMPILE_STRLEN
                    | DUK_COMPILE_NOFILENAME
                    | DUK_COMPILE_SAFE
                    | DUK_COMPILE_NOSOURCE,
            )
        };
        assert_eq!(res, 0); // success
        assert_eq!(unsafe { duk_get_type(pac.ctx, -1) }, 3); // boolean
        assert_eq!(unsafe { duk_get_boolean(pac.ctx, -1) }, 0);
    }

    #[test]
    fn test_is_resolvable_true() {
        let pac = PAC::new();
        let res = unsafe {
            duk_eval_raw(
                pac.ctx,
                CString::new("isResolvable(\"google.com\")")
                    .unwrap()
                    .into_raw(),
                0,
                0 | DUK_COMPILE_EVAL
                    | DUK_COMPILE_STRLEN
                    | DUK_COMPILE_NOFILENAME
                    | DUK_COMPILE_SAFE
                    | DUK_COMPILE_NOSOURCE,
            )
        };
        assert_eq!(res, 0); // success
        assert_eq!(unsafe { duk_get_type(pac.ctx, -1) }, 3); // boolean
        assert_eq!(unsafe { duk_get_boolean(pac.ctx, -1) }, 1);
    }

    #[test]
    fn test_is_resolvable_false() {
        let pac = PAC::new();
        let res = unsafe {
            duk_eval_raw(
                pac.ctx,
                CString::new("isResolvable(\"some-unknown-host-name.com\")")
                    .unwrap()
                    .into_raw(),
                0,
                0 | DUK_COMPILE_EVAL
                    | DUK_COMPILE_STRLEN
                    | DUK_COMPILE_NOFILENAME
                    | DUK_COMPILE_SAFE
                    | DUK_COMPILE_NOSOURCE,
            )
        };
        assert_eq!(res, 0); // success
        assert_eq!(unsafe { duk_get_type(pac.ctx, -1) }, 3); // boolean
        assert_eq!(unsafe { duk_get_boolean(pac.ctx, -1) }, 0);
    }

    #[test]
    fn test_dns_resolve_true() {
        let pac = PAC::new();
        let res = unsafe {
            duk_eval_raw(
                pac.ctx,
                CString::new("dnsResolve(\"google.com\")")
                    .unwrap()
                    .into_raw(),
                0,
                0 | DUK_COMPILE_EVAL
                    | DUK_COMPILE_STRLEN
                    | DUK_COMPILE_NOFILENAME
                    | DUK_COMPILE_SAFE
                    | DUK_COMPILE_NOSOURCE,
            )
        };
        assert_eq!(res, 0); // success
        assert_eq!(unsafe { duk_get_type(pac.ctx, -1) }, 5); // string
        assert!(if let Ok(ip) = unsafe {
            CStr::from_ptr(duk_get_string(pac.ctx, -1))
                .to_string_lossy()
                .parse::<std::net::IpAddr>()
        } {
            ip.is_ipv4()
        } else {
            false
        });
    }

    #[test]
    fn test_dns_resolve_false() {
        let pac = PAC::new();
        let res = unsafe {
            duk_eval_raw(
                pac.ctx,
                CString::new("dnsResolve(\"some-unknown-host-name.com\")")
                    .unwrap()
                    .into_raw(),
                0,
                0 | DUK_COMPILE_EVAL
                    | DUK_COMPILE_STRLEN
                    | DUK_COMPILE_NOFILENAME
                    | DUK_COMPILE_SAFE
                    | DUK_COMPILE_NOSOURCE,
            )
        };
        assert_eq!(res, 0); // success
        assert_eq!(unsafe { duk_get_type(pac.ctx, -1) }, 5); // string
        assert_eq!(
            unsafe { CStr::from_ptr(duk_get_string(pac.ctx, -1)).to_string_lossy() },
            ""
        );
    }

    #[test]
    fn test_dns_domain_levels() {
        let pac = PAC::new();
        let res = unsafe {
            duk_eval_raw(
                pac.ctx,
                CString::new("dnsDomainLevels(\"some.unknown.host.name.com\")")
                    .unwrap()
                    .into_raw(),
                0,
                0 | DUK_COMPILE_EVAL
                    | DUK_COMPILE_STRLEN
                    | DUK_COMPILE_NOFILENAME
                    | DUK_COMPILE_SAFE
                    | DUK_COMPILE_NOSOURCE,
            )
        };
        assert_eq!(res, 0); // success
        assert_eq!(unsafe { duk_get_type(pac.ctx, -1) }, 4); // integer
        assert_eq!(unsafe { duk_get_int(pac.ctx, -1) }, 4);
    }

    #[test]
    fn test_my_ip_address() {
        let pac = PAC::new();
        let res = unsafe {
            duk_eval_raw(
                pac.ctx,
                CString::new("myIpAddress()").unwrap().into_raw(),
                0,
                0 | DUK_COMPILE_EVAL
                    | DUK_COMPILE_STRLEN
                    | DUK_COMPILE_NOFILENAME
                    | DUK_COMPILE_SAFE
                    | DUK_COMPILE_NOSOURCE,
            )
        };
        assert_eq!(res, 0); // success
        assert_eq!(unsafe { duk_get_type(pac.ctx, -1) }, 5); // string
        assert!(if let Ok(ip) = unsafe {
            CStr::from_ptr(duk_get_string(pac.ctx, -1))
                .to_string_lossy()
                .parse::<std::net::IpAddr>()
        } {
            ip.is_ipv4() && !ip.is_loopback()
        } else {
            false
        });
    }

    #[test]
    fn test_is_in_net_true() {
        let pac = PAC::new();
        let res = unsafe {
            duk_eval_raw(
                pac.ctx,
                CString::new("isInNet(\"192.168.12.5\", \"192.168.12.0\", \"255.255.255.0\")")
                    .unwrap()
                    .into_raw(),
                0,
                0 | DUK_COMPILE_EVAL
                    | DUK_COMPILE_STRLEN
                    | DUK_COMPILE_NOFILENAME
                    | DUK_COMPILE_SAFE
                    | DUK_COMPILE_NOSOURCE,
            )
        };
        assert_eq!(res, 0); // success
        assert_eq!(unsafe { duk_get_type(pac.ctx, -1) }, 3); // boolean
        assert_eq!(unsafe { duk_get_boolean(pac.ctx, -1) }, 1);
    }

    #[test]
    fn test_is_in_net_false() {
        let pac = PAC::new();
        let res = unsafe {
            duk_eval_raw(
                pac.ctx,
                CString::new("isInNet(\"192.168.12.5\", \"192.168.17.0\", \"255.255.255.0\")")
                    .unwrap()
                    .into_raw(),
                0,
                0 | DUK_COMPILE_EVAL
                    | DUK_COMPILE_STRLEN
                    | DUK_COMPILE_NOFILENAME
                    | DUK_COMPILE_SAFE
                    | DUK_COMPILE_NOSOURCE,
            )
        };
        assert_eq!(res, 0); // success
        assert_eq!(unsafe { duk_get_type(pac.ctx, -1) }, 3); // boolean
        assert_eq!(unsafe { duk_get_boolean(pac.ctx, -1) }, 0);
    }

    #[test]
    fn test_sh_exp_match_true() {
        let pac = PAC::new();
        let res = unsafe {
            duk_eval_raw(
                pac.ctx,
                CString::new("shExpMatch(\"www.alma.com\",\"www.*\")")
                    .unwrap()
                    .into_raw(),
                0,
                0 | DUK_COMPILE_EVAL
                    | DUK_COMPILE_STRLEN
                    | DUK_COMPILE_NOFILENAME
                    | DUK_COMPILE_SAFE
                    | DUK_COMPILE_NOSOURCE,
            )
        };
        assert_eq!(res, 0); // success
        assert_eq!(unsafe { duk_get_type(pac.ctx, -1) }, 3); // boolean
        assert_eq!(unsafe { duk_get_boolean(pac.ctx, -1) }, 1);
    }

    #[test]
    fn test_sh_exp_match_false() {
        let pac = PAC::new();
        let res = unsafe {
            duk_eval_raw(
                pac.ctx,
                CString::new("shExpMatch(\"www.alma.com\",\"*.alma.org\")")
                    .unwrap()
                    .into_raw(),
                0,
                0 | DUK_COMPILE_EVAL
                    | DUK_COMPILE_STRLEN
                    | DUK_COMPILE_NOFILENAME
                    | DUK_COMPILE_SAFE
                    | DUK_COMPILE_NOSOURCE,
            )
        };
        assert_eq!(res, 0); // success
        assert_eq!(unsafe { duk_get_type(pac.ctx, -1) }, 3); // boolean
        assert_eq!(unsafe { duk_get_boolean(pac.ctx, -1) }, 0);
    }

    #[test]
    fn test_local_host_or_domain_is_true() {
        let pac = PAC::new();
        let res = unsafe {
            duk_eval_raw(
                pac.ctx,
                CString::new("localHostOrDomainIs(\"www\",\"www.alma.com\")")
                    .unwrap()
                    .into_raw(),
                0,
                0 | DUK_COMPILE_EVAL
                    | DUK_COMPILE_STRLEN
                    | DUK_COMPILE_NOFILENAME
                    | DUK_COMPILE_SAFE
                    | DUK_COMPILE_NOSOURCE,
            )
        };
        assert_eq!(res, 0); // success
        assert_eq!(unsafe { duk_get_type(pac.ctx, -1) }, 3); // boolean
        assert_eq!(unsafe { duk_get_boolean(pac.ctx, -1) }, 1);
    }

    #[test]
    fn test_local_host_or_domain_is_full_true() {
        let pac = PAC::new();
        let res = unsafe {
            duk_eval_raw(
                pac.ctx,
                CString::new("localHostOrDomainIs(\"www.alma.com\",\"www.alma.com\")")
                    .unwrap()
                    .into_raw(),
                0,
                0 | DUK_COMPILE_EVAL
                    | DUK_COMPILE_STRLEN
                    | DUK_COMPILE_NOFILENAME
                    | DUK_COMPILE_SAFE
                    | DUK_COMPILE_NOSOURCE,
            )
        };
        assert_eq!(res, 0); // success
        assert_eq!(unsafe { duk_get_type(pac.ctx, -1) }, 3); // boolean
        assert_eq!(unsafe { duk_get_boolean(pac.ctx, -1) }, 1);
    }

    #[test]
    fn test_local_host_or_domain_is_false() {
        let pac = PAC::new();
        let res = unsafe {
            duk_eval_raw(
                pac.ctx,
                CString::new("localHostOrDomainIs(\"www.alma.org\",\"www.alma.com\")")
                    .unwrap()
                    .into_raw(),
                0,
                0 | DUK_COMPILE_EVAL
                    | DUK_COMPILE_STRLEN
                    | DUK_COMPILE_NOFILENAME
                    | DUK_COMPILE_SAFE
                    | DUK_COMPILE_NOSOURCE,
            )
        };
        assert_eq!(res, 0); // success
        assert_eq!(unsafe { duk_get_type(pac.ctx, -1) }, 3); // boolean
        assert_eq!(unsafe { duk_get_boolean(pac.ctx, -1) }, 0);
    }

    #[test]
    fn test_weekday_range_true() {
        let _guard = TEST_LOCAL_MUTEX.lock();
        let mctx = ch::MockChrono::local_context();
        mctx.expect()
            .returning(|| Local.ymd(2019, 10, 8).and_hms(20, 19, 10));
        let pac = PAC::new();
        let res = unsafe {
            duk_eval_raw(
                pac.ctx,
                CString::new("weekdayRange(\"MON\",\"WED\")")
                    .unwrap()
                    .into_raw(),
                0,
                0 | DUK_COMPILE_EVAL
                    | DUK_COMPILE_STRLEN
                    | DUK_COMPILE_NOFILENAME
                    | DUK_COMPILE_SAFE
                    | DUK_COMPILE_NOSOURCE,
            )
        };
        assert_eq!(res, 0); // success
        assert_eq!(unsafe { duk_get_type(pac.ctx, -1) }, 3); // boolean
        assert_eq!(unsafe { duk_get_boolean(pac.ctx, -1) }, 1);
    }

    #[test]
    fn test_weekday_range_false() {
        let _guard = TEST_LOCAL_MUTEX.lock();
        let mctx = ch::MockChrono::local_context();
        mctx.expect()
            .returning(|| Local.ymd(2019, 10, 8).and_hms(20, 19, 10));
        let pac = PAC::new();
        let res = unsafe {
            duk_eval_raw(
                pac.ctx,
                CString::new("weekdayRange(\"WED\",\"FRI\")")
                    .unwrap()
                    .into_raw(),
                0,
                0 | DUK_COMPILE_EVAL
                    | DUK_COMPILE_STRLEN
                    | DUK_COMPILE_NOFILENAME
                    | DUK_COMPILE_SAFE
                    | DUK_COMPILE_NOSOURCE,
            )
        };
        assert_eq!(res, 0); // success
        assert_eq!(unsafe { duk_get_type(pac.ctx, -1) }, 3); // boolean
        assert_eq!(unsafe { duk_get_boolean(pac.ctx, -1) }, 0);
    }

    #[test]
    fn test_weekday_range_utc_true() {
        let _guard = TEST_UTC_MUTEX.lock();
        let mctx = ch::MockChrono::utc_context();
        mctx.expect()
            .returning(|| Utc.ymd(2019, 10, 8).and_hms(18, 19, 10));
        let pac = PAC::new();
        let res = unsafe {
            duk_eval_raw(
                pac.ctx,
                CString::new("weekdayRange(\"MON\",\"WED\",\"GMT\")")
                    .unwrap()
                    .into_raw(),
                0,
                0 | DUK_COMPILE_EVAL
                    | DUK_COMPILE_STRLEN
                    | DUK_COMPILE_NOFILENAME
                    | DUK_COMPILE_SAFE
                    | DUK_COMPILE_NOSOURCE,
            )
        };
        assert_eq!(res, 0); // success
        assert_eq!(unsafe { duk_get_type(pac.ctx, -1) }, 3); // boolean
        assert_eq!(unsafe { duk_get_boolean(pac.ctx, -1) }, 1);
    }

    #[test]
    fn test_weekday_range_utc_false() {
        let _guard = TEST_UTC_MUTEX.lock();
        let mctx = ch::MockChrono::utc_context();
        mctx.expect()
            .returning(|| Utc.ymd(2019, 10, 8).and_hms(18, 19, 10));
        let pac = PAC::new();
        let res = unsafe {
            duk_eval_raw(
                pac.ctx,
                CString::new("weekdayRange(\"WED\",\"FRI\",\"GMT\")")
                    .unwrap()
                    .into_raw(),
                0,
                0 | DUK_COMPILE_EVAL
                    | DUK_COMPILE_STRLEN
                    | DUK_COMPILE_NOFILENAME
                    | DUK_COMPILE_SAFE
                    | DUK_COMPILE_NOSOURCE,
            )
        };
        assert_eq!(res, 0); // success
        assert_eq!(unsafe { duk_get_type(pac.ctx, -1) }, 3); // boolean
        assert_eq!(unsafe { duk_get_boolean(pac.ctx, -1) }, 0);
    }

    #[test]
    fn test_weekday_range_single_true() {
        let _guard = TEST_LOCAL_MUTEX.lock();
        let mctx = ch::MockChrono::local_context();
        mctx.expect()
            .returning(|| Local.ymd(2019, 10, 8).and_hms(20, 19, 10));
        let pac = PAC::new();
        let res = unsafe {
            duk_eval_raw(
                pac.ctx,
                CString::new("weekdayRange(\"TUE\")").unwrap().into_raw(),
                0,
                0 | DUK_COMPILE_EVAL
                    | DUK_COMPILE_STRLEN
                    | DUK_COMPILE_NOFILENAME
                    | DUK_COMPILE_SAFE
                    | DUK_COMPILE_NOSOURCE,
            )
        };
        assert_eq!(res, 0); // success
        assert_eq!(unsafe { duk_get_type(pac.ctx, -1) }, 3); // boolean
        assert_eq!(unsafe { duk_get_boolean(pac.ctx, -1) }, 1);
    }

    #[test]
    fn test_weekday_range_single_false() {
        let _guard = TEST_LOCAL_MUTEX.lock();
        let mctx = ch::MockChrono::local_context();
        mctx.expect()
            .returning(|| Local.ymd(2019, 10, 8).and_hms(20, 19, 10));
        let pac = PAC::new();
        let res = unsafe {
            duk_eval_raw(
                pac.ctx,
                CString::new("weekdayRange(\"WED\")").unwrap().into_raw(),
                0,
                0 | DUK_COMPILE_EVAL
                    | DUK_COMPILE_STRLEN
                    | DUK_COMPILE_NOFILENAME
                    | DUK_COMPILE_SAFE
                    | DUK_COMPILE_NOSOURCE,
            )
        };
        assert_eq!(res, 0); // success
        assert_eq!(unsafe { duk_get_type(pac.ctx, -1) }, 3); // boolean
        assert_eq!(unsafe { duk_get_boolean(pac.ctx, -1) }, 0);
    }

    #[test]
    fn test_weekday_range_single_utc_true() {
        let _guard = TEST_UTC_MUTEX.lock();
        let mctx = ch::MockChrono::utc_context();
        mctx.expect()
            .returning(|| Utc.ymd(2019, 10, 8).and_hms(18, 19, 10));
        let pac = PAC::new();
        let res = unsafe {
            duk_eval_raw(
                pac.ctx,
                CString::new("weekdayRange(\"TUE\",\"GMT\")")
                    .unwrap()
                    .into_raw(),
                0,
                0 | DUK_COMPILE_EVAL
                    | DUK_COMPILE_STRLEN
                    | DUK_COMPILE_NOFILENAME
                    | DUK_COMPILE_SAFE
                    | DUK_COMPILE_NOSOURCE,
            )
        };
        assert_eq!(res, 0); // success
        assert_eq!(unsafe { duk_get_type(pac.ctx, -1) }, 3); // boolean
        assert_eq!(unsafe { duk_get_boolean(pac.ctx, -1) }, 1);
    }

    #[test]
    fn test_weekday_range_single_utc_false() {
        let _guard = TEST_UTC_MUTEX.lock();
        let mctx = ch::MockChrono::utc_context();
        mctx.expect()
            .returning(|| Utc.ymd(2019, 10, 8).and_hms(18, 19, 10));
        let pac = PAC::new();
        let res = unsafe {
            duk_eval_raw(
                pac.ctx,
                CString::new("weekdayRange(\"WED\",\"GMT\")")
                    .unwrap()
                    .into_raw(),
                0,
                0 | DUK_COMPILE_EVAL
                    | DUK_COMPILE_STRLEN
                    | DUK_COMPILE_NOFILENAME
                    | DUK_COMPILE_SAFE
                    | DUK_COMPILE_NOSOURCE,
            )
        };
        assert_eq!(res, 0); // success
        assert_eq!(unsafe { duk_get_type(pac.ctx, -1) }, 3); // boolean
        assert_eq!(unsafe { duk_get_boolean(pac.ctx, -1) }, 0);
    }
}
