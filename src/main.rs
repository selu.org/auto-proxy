use clap::{App, Arg};
use log::{debug, warn};
use std::io::prelude::*;

pub mod pac;
pub mod server;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    env_logger::init();

    let matches = App::new("Auto-Proxy")
        .version(env!("CARGO_PKG_VERSION"))
        .author("Szabolcs Seláf <selu@selu.org>")
        .arg(
            Arg::with_name("config")
                .short("c")
                .long("config")
                .value_name("CONFIG_FILE")
                .help("config file for defaults")
                .default_value("~/.auto_proxy.yml")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("file")
                .short("f")
                .long("proxy-file")
                .value_name("PROXY_FILE")
                .help("file contains PAC configuration")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("url")
                .short("u")
                .long("proxy-url")
                .value_name("PAC_URL")
                .help("URL to PAC configuration")
                .default_value("http://wpad/wpad.dat")
                .overrides_with("file")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("verbosity")
                .short("v")
                .long("verbose")
                .multiple(true)
                .help("Sets the level of verbosity"),
        )
        .get_matches();

    let mut pac = pac::PAC::new();
    let mut js = String::new();
        if let Some(filename) = matches.value_of("file") {
            debug!("proxy-file: {:?}", filename);
            let mut file = std::fs::File::open(filename).unwrap();
            file.read_to_string(&mut js).unwrap();
        } else if let Some(url) = matches.value_of("url") {
            debug!("proxy-url: {:?}", url);
            match get_pac(url).await {
                Ok(content) => js = content,
                Err(e) => {
                   js = String::from(r#"function FindProxyForURL(url,host) { return "DIRECT"; };"#);
                   warn!("pac get error: {}", e);
                }
            }
        } else {
            js = String::from(r#"function FindProxyForURL(url,host) { return "DIRECT"; };"#);
        }

    pac.setup(&js).unwrap();
    Ok(server::start(pac).await)
}

async fn get_pac(url: &str) -> Result<String, reqwest::Error> {
    Ok(reqwest::get(url).await?.text().await?)
}
